const jwt = require("jsonwebtoken");
const router = require("koa-router")();

router.use("/", async (ctx, next) => {
  try {
    // 解密用户信息
    const token = ctx.header.token || ctx.query.token;
    if (!token) {
      ctx.status = 401;
      ctx.error("授权失败", "Token Unauthorized");
      return false;
    }
    const decoded = jwt.verify(token, ctx.config("jwt.secret"));
    const user = await BaaS.Models.user
      .query({ where: { id: decoded.id } })
      .fetch({ withRelated: ["user_info"] });
    if (!user.status) {
      ctx.status = 401;
      ctx.error("用户被禁用", "user Status Unauthorized");
      return false;
    }
    ctx.user = user;
  } catch (err) {
    console.log(err);
    ctx.status = 401;
    ctx.error("授权失败", "Token Unauthorized");
    return false;
  }

  await next();
});
router.use("/app", async (ctx, next) => {
  // 解密baas
  const baasToken = ctx.header.baastoken || ctx.query.baastoken;
  const user = ctx.user;
  if (!baasToken) {
    ctx.status = 401;
    ctx.error("授权失败", "baasToken Unauthorized");
    return false;
  }
  try {
    const baasDecoded = jwt.verify(baasToken, ctx.config("jwt.secret"));
    const baas = await BaaS.Models.baas
      .query({ where: { id: baasDecoded.id } })
      .fetch();

    const userBaas = await BaaS.Models.user_baas
      .query({ where: { baas_id: baas.id, user_id: user.id } })
      .fetch();
    if (!userBaas) {
      ctx.status = 401;
      ctx.error("不存在该应用");
      return false;
    }
    if (!baas.status) {
      ctx.status = 401;
      ctx.error("应用被禁用", "baas Status Unauthorized");
      return false;
    }
    ctx.baas = baas;
    ctx.bookshelf = ctx.getBookshelf();
  } catch (err) {
    ctx.status = 401;
    ctx.error("授权失败", "baasToken Unauthorized");
    return false;
  }
  await next();
});
router.use("/pay/", async (ctx, next) => {
  await next();
});

module.exports = router;

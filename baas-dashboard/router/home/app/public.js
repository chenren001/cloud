const router = require("koa-router")();
/**
 *
 * @api {get} /home/app/baasInfo 获取应用信息
 *
 * apiParam {Number} baas_id 应用id
 *
 *
 */
router.get("/baasInfo", async (ctx, next) => {
  const user = ctx.user;
  const baasInfo = ctx.baas;
  if (!user.id) {
    ctx.error("参数有误");
    return;
  }

  ctx.success({
    name: baasInfo.name,
    appid: baasInfo.appid,
    appkey: baasInfo.appkey
  });
});
module.exports = router;
